//
//  ViewController.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//

//Main Font: Myriad Pro
//https://www.youtube.com/watch?v=CLmOoHzIekw
//https://stackoverflow.com/questions/48387293/save-load-files-in-app-with-swift-4
//https://stackoverflow.com/questions/38041688/how-to-change-button-image-in-swift
//https://stackoverflow.com/questions/26311000/how-to-connect-viewcontroller-swift-to-viewcontroller-in-storyboard

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var Startbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Startbtn.setImage(UIImage(named: "Start_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Startbtn.setImage(UIImage(named: "Start_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

