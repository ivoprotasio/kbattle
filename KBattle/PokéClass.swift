//
//  PokéClass.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//

import Foundation

class Pokemon {
    //Características do Pokémon
    private(set) var nome:String
    private(set) var hp:Int
    private(set) var currenthp:Int
    private(set) var atk:Int
    private(set) var def:Int
    private(set) var spa:Int
    private(set) var spd:Int
    private(set) var spe:Int
    private(set) var lvl:Int
    private(set) var base_xp:Int
    private(set) var xp:Int
    private(set) var type:String
    private(set) var trainer_v:String
    private(set) var opponent_v:String
    private(set) var indexnum:String
    private(set) var moveset:[Move]
    
    var pokemoveset:[Move] {
        set {
            moveset = newValue
        }
        get {
            return moveset
        }
    }
    var xpUpdate:Int {
        set {
            xp = newValue
        }
        get {
            return xp
        }
    }
    var lvlUpdate:Int {
        set {
            lvl = newValue
        }
        get {
            return lvl
        }
    }
    var hpUpdate:Int {
        set {
            currenthp = newValue
        }
        get {
            return currenthp
        }
    }
    
    init(nome:String, hp:Int, currenthp:Int = 0, atk:Int, def:Int, spa:Int, spd:Int, spe:Int, lvl:Int = 5, base_xp:Int, xp:Int = 0, type:String, trainer_v:String, opponent_v:String, indexnum:String, moveset:[Move] = []) {
        self.nome = nome
        self.hp = hp
        self.currenthp = hp
        self.atk = atk
        self.def = def
        self.spa = spa
        self.spd = spd
        self.spe = spe
        self.lvl = lvl
        self.base_xp = base_xp
        self.xp = xp
        self.type = type
        self.trainer_v = trainer_v
        self.opponent_v = opponent_v
        self.indexnum = indexnum
        self.moveset = moveset
    }
    
    //Calculo para Status HP
    func calcHPStatus() -> Int {
        let calc = ((((hp*2)+100)*lvl)/100)+10
        return calc
    }
    //Calculo para Status Ataque
    func calcATKStatus() -> Int {
        let calc = (((atk*2)*lvl)/100)+5
        return calc
    }
    //Calculo para Status Defesa
    func calcDEFStatus() -> Int {
        let calc = (((def*2)*lvl)/100)+5
        return calc
    }
    //Calculo para Status Ataque Especial
    func calcSPAStatus() -> Int {
        let calc = (((spa*2)*lvl)/100)+5
        return calc
    }
    //Calculo para Status Defesa Especial
    func calcSPDStatus() -> Int {
        let calc = (((spd*2)*lvl)/100)+5
        return calc
    }
    //Calculo para Status Rapidez
    func calcSPEStatus() -> Int {
        let calc = (((spe*2)*lvl)/100)+5
        return calc
    }
    //Calculo para Experiência do Pokémon
    func calcEXP() -> Int {
        let calc = 1.5*Double(base_xp)*Double(lvl)
        let calc2 = calc/7.0
        return Int(calc2)
    }
}

class Move {
    //Características do Move
    private(set) var nome:String
    private(set) var pp:Int
    private(set) var power:Int
    private(set) var type:String
    
    init(nome:String, pp:Int, power:Int, type:String) {
        self.nome = nome
        self.pp = pp
        self.power = power
        self.type = type
    }
}

class Potion {
    //Banco de Potions
    private(set) var hp:Int
    
    init(hp:Int) {
        self.hp = hp
    }
}

