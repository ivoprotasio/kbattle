//
//  ChoosePokemon.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//links úteis
//http://swiftdeveloperblog.com/code-examples/sort-array-of-custom-objects-in-swift/
//
import Foundation

import UIKit

class ChoosePokemon: UIViewController {
    
    @IBOutlet weak var Nextbtn: UIButton!
    //Choosen Pokémon Preview
    @IBOutlet weak var Pokemon_Preview:UIImageView!
    @IBOutlet weak var P_Preview_Name: UILabel!
    @IBOutlet weak var P_Preview_Type: UILabel!
    @IBOutlet weak var P_Preview_Level: UILabel!
    //Pokémon List
    @IBOutlet weak var Choose1: UIButton!
    @IBOutlet weak var Choose2: UIButton!
    @IBOutlet weak var Choose3: UIButton!
    var pick:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Nextbtn.setImage(UIImage(named: "Next_sleep")?.withRenderingMode(.alwaysOriginal), for: .normal)
        Nextbtn.setImage(UIImage(named: "Next_active")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
        Nextbtn.isEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func picker(_ sender: UIButton) {
        MyVariables.equipa.append(MyVariables.pokemon[pick])
        MyVariables.equipa.append(MyVariables.pokemon[20])
        MyVariables.equipa.append(MyVariables.pokemon[2])
        MyVariables.pokedex.append(MyVariables.pokemon[0])
        MyVariables.pokedex.append(MyVariables.pokemon[3])
        MyVariables.pokedex.append(MyVariables.pokemon[6])
        
        performSegue(withIdentifier: "mySegue", sender: self)
    }
    @IBAction func Choose_1(_ sender: AnyObject) {
        self.Pokemon_Preview.image = UIImage(named: "001")
        
        let pk = MyVariables.pokemon[0]
        self.P_Preview_Name.text = pk.nome
        self.P_Preview_Type.text = pk.type
        self.P_Preview_Level.text = "\(pk.lvl)"
        
        Choose1.isEnabled = false
        Choose2.isEnabled = true
        Choose3.isEnabled = true
        
        pick = 0
        Nextbtn.isEnabled = true
    }
    
    @IBAction func Choose_2(_ sender: AnyObject) {
        self.Pokemon_Preview.image = UIImage(named: "004")
        
        let pk = MyVariables.pokemon[3]
        self.P_Preview_Name.text = pk.nome
        self.P_Preview_Type.text = pk.type
        self.P_Preview_Level.text = "\(pk.lvl)"
        
        Choose1.isEnabled = true
        Choose2.isEnabled = false
        Choose3.isEnabled = true
        
        pick = 3
        Nextbtn.isEnabled = true
    }
    
    @IBAction func Choose_3(_ sender: AnyObject) {
        self.Pokemon_Preview.image = UIImage(named: "007")
        
        let pk = MyVariables.pokemon[6]
        self.P_Preview_Name.text = pk.nome
        self.P_Preview_Type.text = pk.type
        self.P_Preview_Level.text = "\(pk.lvl)"
        
        Choose1.isEnabled = true
        Choose2.isEnabled = true
        Choose3.isEnabled = false
        
        pick = 6
        Nextbtn.isEnabled = true
    }
}
