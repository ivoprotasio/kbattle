//
//  Battle.swift
//  KBattle
//
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//
//Importante https://digitalleaves.com/blog/2017/02/sequence-hacking-in-swift-i-map-flatmap-sort-filter-reduce/

import Foundation

import UIKit

class Battle: UIViewController {
    
    @IBOutlet weak var Fightbtn: UIButton!
    @IBOutlet weak var Bagbtn: UIButton!
    @IBOutlet weak var Pokemonbtn: UIButton!
    @IBOutlet weak var Runbtn: UIButton!
    @IBOutlet weak var NarrativeText: UILabel!
    
    @IBOutlet weak var ourPoke: UILabel!
    @IBOutlet weak var ourHP: UILabel!
    @IBOutlet weak var ourLvl: UILabel!
    @IBOutlet weak var imgTrainer: UIImageView!
    @IBOutlet weak var ourHPBar: UIProgressView!
    
    @IBOutlet weak var NPCPoke: UILabel!
    @IBOutlet weak var NPCLvl: UILabel!
    @IBOutlet weak var imgNPC: UIImageView!
    @IBOutlet weak var NPChpbar: UIProgressView!
    
    var move = MyVariables.move
    var bag = MyVariables.potion
    var lvlcap:Int = 125
    var firsttime:Bool = true
    var vez:Bool = true
    var terminar = false
    
    //TRAINER
    var team = MyVariables.equipa
    var moveset:[Move] = [Move(nome: "", pp: 0, power: 0, type: "")]
    var maxHP:Int = 0
    var current_hp:Int = 0
    var current_atk:Int = 0
    var current_def:Int = 0
    var current_spa:Int = 0
    var current_spd:Int = 0
    var current_spe:Int = 0
    var current_lvl:Int = 0
    var current_xp:Int = 0
    var moveID:Int = 0
    var move1 = 0
    var move2 = 0
    var move3 = 0
    var move4 = 0
    var sw:Float = 0
    var newvida:Int = 0
    var dmg:Float = 0
    
    //NPC
    var npc = MyVariables.pokemon
    var npcmoveset:[Move] = [Move(nome: "", pp: 0, power: 0, type: "")]
    var npc_maxHP:Int = 0
    var npc_newvida:Int = 0
    var npc_current_hp:Int = 0
    var npc_current_atk:Int = 0
    var npc_current_def:Int = 0
    var npc_current_spa:Int = 0
    var npc_current_spd:Int = 0
    var npc_current_spe:Int = 0
    var npc_current_lvl:Int = 0
    var npcrandom = 0
    var npcchoose = 0
    var npcmove1 = 0
    var npcmove2 = 0
    var npcmove3 = 0
    var npcmove4 = 0
    var setmoves:[Int] = []
    var npcdmg:Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Layout
        Fightbtn.backgroundColor = .clear
        Fightbtn.layer.cornerRadius = 5
        Fightbtn.layer.borderWidth = 1
        Fightbtn.layer.borderColor = UIColor.red.cgColor
        Bagbtn.backgroundColor = .clear
        Bagbtn.layer.cornerRadius = 5
        Bagbtn.layer.borderWidth = 1
        Bagbtn.layer.borderColor = UIColor.yellow.cgColor
        
        Pokemonbtn.backgroundColor = .clear
        Pokemonbtn.layer.cornerRadius = 5
        Pokemonbtn.layer.borderWidth = 1
        Pokemonbtn.layer.borderColor = UIColor.green.cgColor
        
        Runbtn.backgroundColor = .clear
        Runbtn.layer.cornerRadius = 5
        Runbtn.layer.borderWidth = 1
        Runbtn.layer.borderColor = #colorLiteral(red: 0.08327889897, green: 0.8108460683, blue: 0.956264771, alpha: 1)
        
        imgTrainer.image = UIImage(named: "\(team[0].trainer_v)")
        ourPoke.text = team[0].nome
        ourLvl.text = "Lv." + String(team[0].lvl)
        ourHPBar.progress = 1
        ourHP.text = "\(current_hp)"+"/"+"\(current_hp)"
        
        //NPC
        //Stats
        RandomNPC()
        
        npc_current_hp = npc[npcrandom].calcHPStatus()
        npc_maxHP = npc_current_hp
        npc_current_atk = npc[npcrandom].calcATKStatus()
        npc_current_def = npc[npcrandom].calcDEFStatus()
        npc_current_spa = npc[npcrandom].calcSPAStatus()
        npc_current_spd = npc[npcrandom].calcSPDStatus()
        npc_current_spe = npc[npcrandom].calcSPEStatus()
        
        //Graphics
        imgNPC.image = UIImage(named: "\(npc[npcrandom].opponent_v)")
        NPCPoke.text = npc[npcrandom].nome
        NPCLvl.text = "Lv." + String(npc[npcrandom].lvl)
        NPChpbar.progress = 1
        //Ataques NPC
        setmoves = randomMove()
        npcmoveset = [move[setmoves[0]], move[setmoves[1]], move[setmoves[2]], move[setmoves[3]]]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        team = MyVariables.equipa
        NarrativeText.text = "Choose an option."
        
        //OUR POKEMON
        //Stats
        current_hp = team[0].calcHPStatus()
        current_atk = team[0].calcATKStatus()
        current_def = team[0].calcDEFStatus()
        current_spa = team[0].calcSPAStatus()
        current_spd = team[0].calcSPDStatus()
        current_spe = team[0].calcSPEStatus()
        current_lvl = team[0].lvl
        current_xp = team[0].xp
        maxHP = current_hp
        //Graphics
        imgTrainer.image = UIImage(named: "\(team[0].trainer_v)")
        ourPoke.text = team[0].nome
        ourLvl.text = "Lv." + String(team[0].lvl)
        ourHPBar.progress = 1
        ourHP.text = "\(current_hp)"+"/"+"\(current_hp)"
        //Ataques pokemon
        if(team[0].moveset.isEmpty){
            setmoves = randomMove()
            moveset = [move[setmoves[0]], move[setmoves[1]], move[setmoves[2]], move[setmoves[3]]]
            team[0].pokemoveset = moveset
        }else{
            moveset = team[0].pokemoveset
        }
        
        //PokeDex
        let pk = MyVariables.pokemon[npcrandom]
        if(!(MyVariables.pokedex.contains {$0.nome == pk.nome })){
            MyVariables.pokedex.append(pk)
        }
    }
    //Func para Trainer
    func randomMove() -> ([Int]){
        repeat {
            move1 = Int(arc4random_uniform(82))
            move2 = Int(arc4random_uniform(82))
            move3 = Int(arc4random_uniform(82))
            move4 = Int(arc4random_uniform(82))
        } while (move1 == move2 || move1 == move3 || move1 == move4 || move2 == move3 || move2 == move4 || move3 == move4)
        let setmoves = [move1, move2, move3, move4]
        return setmoves
    }
    //Func para NPC
    func RandomNPC(){
        npcrandom = Int(arc4random_uniform(46))
    }
    func RandomNPCAttack(){
        npcchoose = Int(arc4random_uniform(3))
    }
    //Func para calculo de Fraquesas
    func Types(){
        if (moveset[moveID].type == npc[0].type){
            sw = 1
        }else if(String(moveset[moveID].type) == "fire" && String(npc[0].type) == "grass"){
            sw = 2
        }else if(String(moveset[moveID].type) == "fire" && String(npc[0].type) == "steel"){
            sw = 2
        }else if(String(moveset[moveID].type) == "fire" && String(npc[0].type) == "bug"){
            sw = 2
        }else if(String(moveset[moveID].type) == "fire" && String(npc[0].type) == "water"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "water" && String(npc[0].type) == "fire"){
            sw = 2
        }else if(String(moveset[moveID].type) == "water" && String(npc[0].type) == "rock"){
            sw = 2
        }else if(String(moveset[moveID].type) == "water" && String(npc[0].type) == "ground"){
            sw = 2
        }else if(String(moveset[moveID].type) == "water" && String(npc[0].type) == "grass"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "grass" && String(npc[0].type) == "water"){
            sw = 2
        }else if(String(moveset[moveID].type) == "grass" && String(npc[0].type) == "rock"){
            sw = 2
        }else if(String(moveset[moveID].type) == "grass" && String(npc[0].type) == "ground"){
            sw = 2
        }else if(String(moveset[moveID].type) == "grass" && String(npc[0].type) == "fire"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "normal" && String(npc[0].type) == "ghost"){
            sw = 0
        }else if(String(moveset[moveID].type) == "fighting" && String(npc[0].type) == "normal"){
            sw = 2
        }else if(String(moveset[moveID].type) == "fighting" && String(npc[0].type) == "steel"){
            sw = 2
        }else if(String(moveset[moveID].type) == "fighting" && String(npc[0].type) == "dark"){
            sw = 2
        }else if(String(moveset[moveID].type) == "fighting" && String(npc[0].type) == "ghost"){
            sw = 0
        }else if(String(moveset[moveID].type) == "ice" && String(npc[0].type) == "grass"){
            sw = 2
        }else if(String(moveset[moveID].type) == "ice" && String(npc[0].type) == "steel"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "ice" && String(npc[0].type) == "fire"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "electric" && String(npc[0].type) == "water"){
            sw = 2
        }else if(String(moveset[moveID].type) == "electric" && String(npc[0].type) == "flying"){
            sw = 2
        }else if(String(moveset[moveID].type) == "electric" && String(npc[0].type) == "ground"){
            sw = 0
        }else if(String(moveset[moveID].type) == "flying" && String(npc[0].type) == "grass"){
            sw = 2
        }else if(String(moveset[moveID].type) == "flying" && String(npc[0].type) == "bug"){
            sw = 2
        }else if(String(moveset[moveID].type) == "flying" && String(npc[0].type) == "rock"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "poison" && String(npc[0].type) == "grass"){
            sw = 2
        }else if(String(moveset[moveID].type) == "poison" && String(npc[0].type) == "psychic"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "bug" && String(npc[0].type) == "grass"){
            sw = 2
        }else if(String(moveset[moveID].type) == "bug" && String(npc[0].type) == "fire"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "dark" && String(npc[0].type) == "psychic"){
            sw = 2
        }else if(String(moveset[moveID].type) == "dark" && String(npc[0].type) == "fighting"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "psychic" && String(npc[0].type) == "fighting"){
            sw = 2
        }else if(String(moveset[moveID].type) == "psychic" && String(npc[0].type) == "poison"){
            sw = 2
        }else if(String(moveset[moveID].type) == "rock" && String(npc[0].type) == "flying"){
            sw = 2
        }else if(String(moveset[moveID].type) == "rock" && String(npc[0].type) == "steel"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "rock" && String(npc[0].type) == "fire"){
            sw = 2
        }else if(String(moveset[moveID].type) == "rock" && String(npc[0].type) == "water"){
            sw = 0.5
        }else if(String(moveset[moveID].type) == "ground" && String(npc[0].type) == "rock"){
            sw = 2
        }else if(String(moveset[moveID].type) == "ground" && String(npc[0].type) == "steel"){
            sw = 2
        }else if(String(moveset[moveID].type) == "ground" && String(npc[0].type) == "fire"){
            sw = 2
        }else if(String(moveset[moveID].type) == "ground" && String(npc[0].type) == "electric"){
            sw = 2
        }else if(String(moveset[moveID].type) == "ground" && String(npc[0].type) == "flying"){
            sw = 0
        }else if(String(moveset[moveID].type) == "ghost" && String(npc[0].type) == "psychic"){
            sw = 2
        }else if(String(moveset[moveID].type) == "ghost" && String(npc[0].type) == "dark"){
            sw = 0.5
        }else{
            sw = 1
        }
    }
    //Func para calculo de Dano
    func Damage(Fcurrent_lvl:Int, Fpower:Int, Fcurrent_def:Int) -> Float {
        Types()
        let lvlcalc:Int = ((2*current_lvl)/5+2)
        let powercalc:Int = lvlcalc*current_atk*moveset[moveID].power/current_def
        let lastcalc:Int = powercalc/50+2
        let calc:Float = Float(lastcalc)*sw
        return calc
    }
    func DamageNPC(Fcurrent_lvl:Int, Fpower:Int, Fcurrent_def:Int) -> Float {
        Types()
        let lvlcalc:Int = ((2*npc_current_lvl)/5+2)
        let powercalc:Int = lvlcalc*npc_current_atk*npcmoveset[0].power/npc_current_def
        let lastcalc:Int = powercalc/50+2
        let calc:Float = Float(lastcalc)*sw
        return Float(calc)
    }
    
    //Func para Round da Batalha
    func Battle(MOVEID:Int) {
        
        if(firsttime == true){
            firsttime = false
            RandomNPCAttack()
            if(current_spe < npc_current_spe){
                vez = true
                NPCAttack()
            }else if(current_spe > npc_current_spe){
                vez = false
                TrainerAttack()
            }
        }else{
            if(vez == true){
                vez = false
                NPCAttack()
            }else{
                vez = true
                TrainerAttack()
            }
        }
        
        //Reset Button Tag
        Fightbtn.setTitle("FIGHT", for: .normal)
        Bagbtn.setTitle("BAG", for: .normal)
        Pokemonbtn.setTitle("POKEMON", for: .normal)
        Runbtn.setTitle("RUN", for: .normal)
    }
    
    func NPCAttack(){
        npcdmg = DamageNPC(Fcurrent_lvl: npc_current_lvl, Fpower: move[npcchoose].power, Fcurrent_def: current_def)
        newvida = Int(Float(current_hp)-npcdmg)
        if (newvida < 0){
            current_hp = 0
            team[0].hpUpdate = current_hp
            print("Perdeu :(")
            terminar = true
        }else {
            current_hp = newvida
            team[0].hpUpdate = current_hp
            dmg = Damage(Fcurrent_lvl: current_lvl, Fpower: move[moveID].power, Fcurrent_def: npc_current_def)
            npc_newvida = Int(Float(npc_current_hp)-dmg)
        }
        if (npc_newvida < 0){
            npc_current_hp = 0
            print("Ganhou!")
            terminar = true
            addExp()
          
        }else {
            npc_current_hp = npc_newvida
        }
        ourHP.text = String(current_hp) + "/"+"\(maxHP)"
        ourHPBar.setProgress((Float(current_hp) / Float(maxHP)), animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.NPChpbar.setProgress((Float(self.npc_current_hp) / Float(self.npc_maxHP)), animated: true)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            self.close()
        }
        
    }
    func addExp(){
        current_xp = team[0].calcEXP() + current_xp
        team[0].xpUpdate = current_xp
        NarrativeText.text = "Experience received: \(team[0].calcEXP())\n"
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            if(self.current_xp >= self.lvlcap){
                self.team[0].lvlUpdate = self.current_lvl + 1
                self.current_xp = 0
                self.NarrativeText.text = "You Pokémon Level up to level \(self.current_lvl)\n"
                self.lvlcap = self.lvlcap + 50
            }
        }
    }
    func TrainerAttack(){
        dmg = Damage(Fcurrent_lvl: current_lvl, Fpower: move[moveID].power, Fcurrent_def: npc_current_def)
        npc_newvida = Int(Float(npc_current_hp)-dmg)
        
        if(sw == 2){
            NarrativeText.text = "Super effective"
        }else if(sw == 0.5){
            NarrativeText.text = "Not very effective"
        }else if(sw == 0){
            NarrativeText.text = "Not effective"
        }else{
            NarrativeText.text = "It was effective"
        }
        if (npc_newvida < 0){
            npc_current_hp = 0
            print("Ganhou!")
            terminar = true
            addExp()
        }else {
            npc_current_hp = npc_newvida
            npcdmg = DamageNPC(Fcurrent_lvl: npc_current_lvl, Fpower: move[npcchoose].power, Fcurrent_def: current_def)
            newvida = Int(Float(current_hp)-npcdmg)
        }
        NPChpbar.setProgress((Float(npc_current_hp) / Float(npc_maxHP)), animated: true)
        if (newvida < 0){
            current_hp = 0
            team[0].hpUpdate = current_hp
            print("Perder :(")
            terminar = true
        }else {
            current_hp = newvida
            team[0].hpUpdate = current_hp
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.ourHP.text = String(self.current_hp) + "/"+"\(self.maxHP)"
            self.ourHPBar.setProgress((Float(self.current_hp) / Float(self.maxHP)), animated: true)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            self.close()
        }
    }
    //Action Buttons
    @IBAction func Onclick(_ sender: UIButton){
        
        switch sender.tag {
        case 1:
            if(Fightbtn.currentTitle == "FIGHT"){
                Fightbtn.setTitle("\(moveset[0].nome)", for: .normal)
                Bagbtn.setTitle("\(moveset[1].nome)", for: .normal)
                Pokemonbtn.setTitle("\(moveset[2].nome)", for: .normal)
                Runbtn.setTitle("\(moveset[3].nome)", for: .normal)
            }else{
                moveID = 0
                Battle(MOVEID:moveID)
            }
        case 2:
            if(Bagbtn.currentTitle == "BAG"){
                
            }else{
                moveID = 1
                Battle(MOVEID:moveID)
            }
            break;
        case 3:
            if(Pokemonbtn.currentTitle == "POKEMON"){
                performSegue(withIdentifier: "trocar", sender: nil)
            }else{
                moveID = 2
                Battle(MOVEID:moveID)
            }
            break;
        default:
            break
        }
    }
    
    func close (){
        if (terminar == true){
            dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func onClose(_ sender: Any) {
        if(Runbtn.currentTitle == "RUN"){
            dismiss(animated: true, completion: nil)
        }else{
            moveID = 3
            Battle(MOVEID:moveID)
        }
    }
}
