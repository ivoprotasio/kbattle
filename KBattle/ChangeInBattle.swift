//
//  Switch.swift
//  KBattle
//
//  Created by Developer on 05/07/18.
//  Copyright © 2018 Andre&Ivo. All rights reserved.
//
//https://stackoverflow.com/questions/36541764/how-to-rearrange-item-of-an-array-to-new-position-in-swift - VER MAIS TARDE

import Foundation

import UIKit

class Switch: UIViewController {
    
    @IBOutlet weak var Okbtn: UIButton!
    @IBOutlet weak var Backbtn: UIButton!
    //Image View
    @IBOutlet weak var Poke1: UIImageView!
    @IBOutlet weak var Poke2: UIButton!
    @IBOutlet weak var Poke3: UIButton!
    @IBOutlet weak var Poke4: UIButton!
    @IBOutlet weak var Poke5: UIButton!
    @IBOutlet weak var Poke6: UIButton!
    
    //Name View
    @IBOutlet weak var NPoke1: UILabel!
    @IBOutlet weak var NPoke2: UILabel!
    @IBOutlet weak var NPoke3: UILabel!
    @IBOutlet weak var NPoke4: UILabel!
    @IBOutlet weak var NPoke5: UILabel!
    @IBOutlet weak var NPoke6: UILabel!
    
    var team = MyVariables.equipa
    
    var poketag:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Backbtn.backgroundColor = .clear
        Backbtn.layer.cornerRadius = 5
        Backbtn.layer.borderWidth = 1
        Backbtn.layer.borderColor = #colorLiteral(red: 1, green: 0.5204096305, blue: 0.2164450464, alpha: 1)
        
        Okbtn.backgroundColor = .clear
        Okbtn.layer.cornerRadius = 5
        Okbtn.layer.borderWidth = 1
        Okbtn.layer.borderColor = #colorLiteral(red: 1, green: 0.5204096305, blue: 0.2164450464, alpha: 1)
        
        //Team Load
        let pokecount:Int = Int(team.count)
        switch pokecount {
        case 1:
            self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
            self.NPoke1.text = "\(team[0].nome)"
            break
        case 2:
            self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
            self.NPoke1.text = "\(team[0].nome)"
            self.Poke2.setImage(UIImage(named: "\(team[1].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke2.text = "\(team[1].nome)"
            break
        case 3:
            self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
            self.NPoke1.text = "\(team[0].nome)"
            self.Poke2.setImage(UIImage(named: "\(team[1].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke2.text = "\(team[1].nome)"
            self.Poke3.setImage(UIImage(named: "\(team[2].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke3.text = "\(team[2].nome)"
            break
        case 4:
            self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
            self.NPoke1.text = "\(team[0].nome)"
            self.Poke2.setImage(UIImage(named: "\(team[1].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke2.text = "\(team[1].nome)"
            self.Poke3.setImage(UIImage(named: "\(team[2].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke3.text = "\(team[2].nome)"
            self.Poke4.setImage(UIImage(named: "\(team[3].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke4.text = "\(team[3].nome)"
            break
        case 5:
            self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
            self.NPoke1.text = "\(team[0].nome)"
            self.Poke2.setImage(UIImage(named: "\(team[1].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke2.text = "\(team[1].nome)"
            self.Poke3.setImage(UIImage(named: "\(team[2].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke3.text = "\(team[2].nome)"
            self.Poke4.setImage(UIImage(named: "\(team[3].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke5.text = "\(team[3].nome)"
            self.Poke5.setImage(UIImage(named: "\(team[4].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke5.text = "\(team[4].nome)"
            break
        case 6:
            self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
            self.NPoke1.text = "\(team[0].nome)"
            self.Poke2.setImage(UIImage(named: "\(team[1].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke2.text = "\(team[1].nome)"
            self.Poke3.setImage(UIImage(named: "\(team[2].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke3.text = "\(team[2].nome)"
            self.Poke4.setImage(UIImage(named: "\(team[3].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke5.text = "\(team[3].nome)"
            self.Poke5.setImage(UIImage(named: "\(team[4].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke5.text = "\(team[4].nome)"
            self.Poke6.setImage(UIImage(named: "\(team[5].opponent_v)")?.withRenderingMode(.alwaysOriginal), for: .normal)
            self.NPoke6.text = "\(team[5].nome)"
            break
        default:
        self.Poke1.image = UIImage(named: "\(team[0].opponent_v)")
        self.NPoke1.text = "???"
        self.Poke2.setImage(UIImage(named: "000")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.NPoke2.text = "???"
        self.Poke3.setImage(UIImage(named: "000")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.NPoke3.text = "???"
        self.Poke4.setImage(UIImage(named: "000")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.NPoke5.text = "???"
        self.Poke5.setImage(UIImage(named: "000")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.NPoke5.text = "???)"
        self.Poke6.setImage(UIImage(named: "000")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.NPoke6.text = "???"
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClose(_ sender: UIButton) {
        let i = sender.tag
        if(i == 6){
            dismiss(animated: true, completion: nil)
        }else if(i == 0){
            let element = MyVariables.equipa.remove(at: poketag)
            MyVariables.equipa.insert(element, at: 0)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func choosenPoke(sender: UIButton){
        let i = sender.tag
        poketag = i
    }
}
